package com.br.tdd;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Calculadora {
    public static int soma(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public static double soma(List<Double> listaNumerosQuebrados) {
        double resultado = 0;

        for (double item : listaNumerosQuebrados) {
            resultado += item;
        }

        return resultado;
    }

    public static int dividir(int primeiroNumero, int segundoNumero) {
        int resultado;

        if (primeiroNumero > segundoNumero) {
            return resultado = primeiroNumero / segundoNumero;
        } else {
            return resultado = segundoNumero / primeiroNumero;
        }
    }

    public static double dividir(double primeiroNumero, double segundoNumero) {
        BigDecimal resultado = new BigDecimal(primeiroNumero).divide(new BigDecimal(segundoNumero),2, RoundingMode.DOWN);
        return Double.parseDouble(resultado.toString());
    }


    public static int multiplicar(int primeiroNumero, int segundoNumero) {
        return (primeiroNumero * segundoNumero);
    }

    public static double multiplicar(double primeiroNumero, double segundoNumero) {
        BigDecimal resultado = new BigDecimal(primeiroNumero).divide(new BigDecimal(segundoNumero),2, RoundingMode.DOWN);
        return Double.parseDouble(resultado.toString());
    }
}
