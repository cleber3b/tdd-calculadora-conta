package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class CalculadoraTeste {

    //boa pratica
    /*todos mestodo de teste é void sem retorno*/
    //Nome do Metodo deve deixar claro o que ele está testando

/*    @BeforeEach
    public void printaConsole() {
        System.out.println("Subtrai");
        System.out.println(new BigDecimal("2.00").subtract(new BigDecimal("1.1")));

        System.out.println("");
        System.out.println("Soma");
        System.out.println(new BigDecimal("2.00").add(new BigDecimal("1.2")));

        System.out.println("");
        System.out.println("Compara");
        System.out.println(new BigDecimal("2.00").compareTo(new BigDecimal("1.3")));

        System.out.println("");
        System.out.println("Divide");
        System.out.println(new BigDecimal("2.00").divide(new BigDecimal("2.00")));
        System.out.println(new BigDecimal("5.6").divide(new BigDecimal("3.1"),2, RoundingMode.DOWN));
        System.out.println(new BigDecimal("5.6").divide(new BigDecimal("3.1"),2, RoundingMode.UP));

        System.out.println("");
        System.out.println("Máximo");
        System.out.println(new BigDecimal("2.00").max(new BigDecimal("1.5")));

        System.out.println("");
        System.out.println("Mínimo");
        System.out.println(new BigDecimal("2.00").min(new BigDecimal("1.6")));

        System.out.println("");
        System.out.println("Potência");
        System.out.println(new BigDecimal("2.00").pow(2));

        System.out.println("");
        System.out.println("Multiplica");
        System.out.println(new BigDecimal("2.00").multiply(new BigDecimal("1.8")));
        System.out.println("");
    }*/

    @Test
    public void testarSomaDeDoisNumeros() {
        int resultado = Calculadora.soma(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarSomaDeVariosNumeros() {
        List<Double> listaNumeroQuebrados = new ArrayList<>();
        double resultadoEsperado = 0;

        for (int i = 0; i < 10; i++) {
            listaNumeroQuebrados.add(i, 1 + 2.1);
            resultadoEsperado += 1 + 2.1;
        }

        double resultado = Calculadora.soma(listaNumeroQuebrados);

        Assertions.assertEquals(resultadoEsperado, resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosNatural() {
        double resultado = Calculadora.dividir(3, 15);

        Assertions.assertEquals(5, resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosQuebrado() {
        double resultado = Calculadora.dividir(5.6, 3.1);

        Assertions.assertEquals(1.80, resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosNatural() {
        double resultado = Calculadora.multiplicar(3, 15);

        Assertions.assertEquals(45, resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosQuebrado() {
        double resultado = Calculadora.multiplicar(3.1, 5.7);

        Assertions.assertEquals(0.54, resultado);
    }


}
