package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ContaTeste {
    private Cliente cliente;
    private Conta conta;

    //Antes de cada metodo de teste ser executado o Jupiter fará esse cenário criando novamente o cenario de entrada
    @BeforeEach
    public void setUp(){
        this.cliente = new Cliente("Vinicius");
        this.conta = new Conta(this.cliente, 100.00);
    }

    //Exemplo do exercicio de COnta do começo da aula

    @Test
    public void testarDepositoEmConta() {
        double valorDeDeposito = 400.00;
        this.conta.depositar(valorDeDeposito);

        Assertions.assertEquals(500, conta.getSaldo());
    }

    @Test
    public void testarSaqueDeConta() {
        double valorDeSaque = 50.00;
        this.conta.sacar(valorDeSaque);

        Assertions.assertEquals(50, conta.getSaldo());
    }

    @Test
    public void testarSaqueDeContaSemSaldo() {
        double valorDeSaque = 200.00;

        Assertions.assertThrows(RuntimeException.class, /* lambda */ () -> {conta.sacarSemSaldo(valorDeSaque);});
    }

}
